grammar AntlrTaleCodeGrammar_v3;

@header {
    import static alientextengine.talecode.parser.SemanticUtil.*;
    import static alientextengine.talecode.parser.Interpreter.*;
}

/*SYNTATIC*/
history
:
	(
		taledefinitions world newlines links*
	)
	{
            //Interpreter
            endHistory();
        }

;

/*UTIL*/
newlines
:
	NEWLINE*
;

/*GLOBALS TREE*/
taledefinitions
:
	(
		ID ':' STRING newlines
		{
                        if(isValidTaleDefinition($ID.text)){
                              registerCall($ID.text, $ID.getLine());
                              
                              //Interpreter
                              registerHeader($ID.text.trim(), $STRING.text.trim());
                              
                        }else{
                              error("Invalid tale definition key: "+$ID.text, $ID.getLine());
                        }
                   }

	)+
;

/*Schedule actions to the tale driver*/
schedule
:
	newlines
	{
		String id = "";
	}
	SCHEDULE (
		ID { id = $ID.text; }
		|
		defineless_define { id = $defineless_define.name; }
	) EVERY UNSIGNED_INT
	{
		scheduleToTaleDriver(id, $UNSIGNED_INT.text, $SCHEDULE.getLine());
	}
	newlines
;

/* This calls the methods of the definition in the named object that
was defined in the first pass*/
/*
 * define APIID ID assmap end
 * 
 */
define
:
	newlines 'define' definitionType ID newlines
	{
                //Enter a scope of assignment calls
                enterScope($definitionType.IDVAL);
        }

	assignmentmap
	{
                //Leave the assignment scope
                leaveScope();
        }

	'end'
	{
     String name = $ID.text.trim();
     String code = "define "+$definitionType.text+" "+name+" "+$assignmentmap.text+" end";

	//Check avaliability
     if(canDefineGlobal(name)){
        //This runs safe
        defineGlobal(name, code);
        
        //Interpreter
        registerObject(name, $definitionType.IDVAL);
     }else{
        //Store the programmer mistake
        error("Global already defined: "+name, $ID.getLine());
     }

     //Check method calls
     //Interpreter
     int index = 0;
     for(String call : $assignmentmap.methodCalls){
         if(!isApiCallDefinedForAPIID($definitionType.IDVAL, call)){
             error("Key "+call+" isn't defined for the api id "+$definitionType.IDVAL+".", $ID.getLine());
         }else{
             definitionApiCallByName(name, call, $assignmentmap.methodValues.get(index), $ID.getLine());
         }
         index++;
     }
    }

;

/*Defines an item without the define keyword (used inside scenes).
This also adds the defition to the global map using the name*/
defineless_define returns [ String name ] @init { $name = "";}
:
	newlines definitionType ID newlines
	{
                //Enter a scope of assignment calls
                enterScope($definitionType.IDVAL);
                
             String name = $ID.text.trim();
		     String code = "define "+$definitionType.text+" "+name+" "+$assignmentmap.text+" end";
		     $name = $ID.text.trim();
		     
		     //Check avaliability
		     if(canDefineGlobal(name)){
		        //This runs safe
		        defineGlobal(name, code);
		        
		        //Interpreter
		        registerObject(name, $definitionType.IDVAL);
		     }else{
		        //Store the programmer mistake
		        error("Global already defined: "+name, $ID.getLine());
		     }
    }

	assignmentmap
	{
            //Leave the assignment scope
            leaveScope();
    }

	'end' newlines
	{
     /*String name = $ID.text.trim();
     String code = "define "+$definitionType.text+" "+name+" "+$assignmentmap.text+" end";
     $name = $ID.text.trim();*/
     
     //Check method calls
     //Interpreter
     int index = 0;
     for(String call : $assignmentmap.methodCalls){
         if(!isApiCallDefinedForAPIID($definitionType.IDVAL, call)){
             error("Key "+call+" isn't defined for the api id "+$definitionType.IDVAL+".", $ID.getLine());
         }else{
             definitionApiCallByName($ID.text, call, $assignmentmap.methodValues.get(index), $ID.getLine());
         }
         index++;
     }
    }

;

/*Same as the defineless_define but doesn't add the definition to
the global map*/
defineless_define_noname
:
	newlines definitionType newlines
	{
                //Enter a scope of assignment calls
                enterScope($definitionType.IDVAL);
        }

	assignmentmap
	{
                //Leave the assignment scope
                leaveScope();
        }

	'end' newlines
	{
        int index = 0;
        for(String call : $assignmentmap.methodCalls){
            if(!isApiCallDefinedForAPIID($definitionType.IDVAL, call)){
                error("Key "+call+" isn't defined for the api id "+$definitionType.IDVAL+".", $definitionType.line);
            }else{
                 definitionApiCallByTypeCurrent($definitionType.text, call, $assignmentmap.methodValues.get(index), $definitionType.line);
            }
            
            index++;
        }
    }

;

assignmentmap returns
[ ArrayList<String> methodCalls, ArrayList<ArrayList<String>> methodValues, int line ]
@init { $methodCalls = new ArrayList<>(); $methodValues = new ArrayList<>(); $line = 0;}
:
	newlines
	(
		assignment
		{
                //Register the call or an error if double call
                registerCall($assignment.key, $assignment.line);
                $methodCalls.add($assignment.key);
                $methodValues.add($assignment.values);
                $line = $assignment.line;
              }

	)*
;

/*This is a key value map used in each module of definition*/
assignment returns [String key, ArrayList<String> values, int line]
@init {$key=""; $values=new ArrayList<String>(); $line=0;}
:
	(
		ID ':'
		(
			param
			{
				$values.add($param.text.trim());
			}

			| params
			{
				$values = $params.paramList;
			}

		) newlines
		{
			$key = $ID.text.trim();
			$line = $ID.getLine();
		}

	)
	|
	(
		ID NOARGSSEMI newlines
		{ $key = $ID.text.trim(); $values.clear(); $line = $ID.getLine();}

	)
;

apiid returns [ String IDVAL, int line ] @init { $IDVAL=""; $line = 0;}
:
	(
		APIID params?
		{
        if(!isValidAPIID($APIID.text))
            error("Invalid API ID! No such ID in alien text engine: "+$APIID.text, $APIID.getLine());
        else{//Interpreter
             if($params.text != null && $params.text.length() > 0){
                  apiInstantiateQueue($APIID.text, $params.paramList, $APIID.getLine());
             }else{
                  apiInstantiateQueue($APIID.text, null, $APIID.getLine());
             }
        }
        $IDVAL = $APIID.text;
        $line = $APIID.getLine();
    }

	)
;

fromCopy returns [ String IDVAL, int line ] @init { $IDVAL=""; $line = 0;}
:
	FROM ID
	{
	        if(!isObjectInstantiated($ID.text))
	        	error("Object to copy isn't instantiated! ID: "+$ID.text, $ID.getLine());
	        else{
	        	//Copy object and send to api queue
	            apiCopyQueue($ID.text, $ID.getLine());
	            $IDVAL = getApiIDFromDeclaredObject($ID.text);
	        }
	        $line = $FROM.getLine();
    	}

;

definitionType returns [ String IDVAL, int line ]
@init { $IDVAL=""; $line = 0;}
:
	fromCopy
	{ $IDVAL = $fromCopy.IDVAL; $line = $fromCopy.line;}

	| apiid
	{ $IDVAL = $apiid.IDVAL; $line = $apiid.line;}

;

object
:
	ID
;

params returns [ ArrayList<String> paramList ]
@init { $paramList = new ArrayList<>(); }
:
	newlines '[' newlines
	(
		param
		{$paramList.add($param.text);}

		(
			newlines ',' newlines param newlines
			{$paramList.add($param.text);}

		)*
	)? ']'
;

param
:
	(
		copiable
		| apiid
		| STRING
		| number
	)
;

/*WORLD TREE*/
addToHistory
:
	newlines 'install' historyInsertParams
;

historyInsertParams
:
	LBRACKET newlines addable
	{
		//Adds valid objects to history directly
    	addAnyToHistoryByName($addable.name, $LBRACKET.getLine());
    }

	(
		newlines COMMA newlines addable
		{
		//Adds valid objects to history directly
    	addAnyToHistoryByName($addable.name, $COMMA.getLine());   
    }

	)* newlines RBRACKET
;

world
:
	(define | schedule)* addToHistory? newlines scene*
;

scene
:
	'scene' ID newlines
	{
            setCurrentScene($ID.text);
      }

	scenebody newlines 'end' newlines
;

scenebody
:
	startingscene?
	{
                //Enter a scope of assignment calls
                enterScope("scene");
        }

	assignmentmap
	{
                //Leave the assignment scope
                leaveScope();
        }

	insertions? links*
	{
        //Interpreter varaible
        int index = 0;
        //Combined code
        for(String call : $assignmentmap.methodCalls){
            if(!isApiCallDefinedForScene(call)){
                error("Key "+call+" isn't defined for the scene api.", $assignmentmap.line);        
            }else{
                //Interpreter
                sceneApiCall(call, $assignmentmap.methodValues.get(index));
            }
            index++;
        }
      }

;

startingscene
:
	STARTINGSCENE
;

insertions
:
	'add' insertparams
;

insertparams
:
	'[' newlines addable
	{
                    switch($addable.mode){
                    	case 0: //By queue
                    		addObjectByQueue();
                    	break;
                    	case 1: //By name
                    		addObjectByName($addable.name);
                    	break;
                    	case 2: //Copy by name
                    		if(!isObjectInstantiated($addable.name))
	            				error("Object to copy isn't instantiated! ID: "+$addable.name, $COMMA.getLine());
	           				else{
                    			addObjectCopyByName($addable.name);
                    		}
                    	break;
                   		default:
                   			 addObjectByQueue();
                   		break;
                    }    
               }

	(
		newlines COMMA newlines addable
		{
                    switch($addable.mode){
                    	case 0: //By queue
                    		addObjectByQueue();
                    	break;
                    	case 1: //By name
                    		addObjectByName($addable.name);
                    	break;
                    	case 2: //Copy by name
                    		if(!isObjectInstantiated($addable.name))
	            				error("Object to copy isn't instantiated! ID: "+$addable.name, $COMMA.getLine());
	           				else{
                    			addObjectCopyByName($addable.name);
                    		}
                    	break;
                   		default:
                   			 addObjectByQueue();
                   		break;
                    }    
               }

	)* newlines ']'
;

addable returns [ String name, int mode ] @init { $name = ""; $mode = 0; }
/* 0 - add by queue, 1 - add by name, 2 - copy by name*/
:
	(
		defineless_define
		{$name=$defineless_define.name;$mode=1;}

		| defineless_define_noname
		{$name="";$mode=0;}

		| copiable
		{
          if(!isGlobalDefined($copiable.name)){
             error("Can't add undefined object: "+$copiable.name, $copiable.line);  
          }
          $name=$copiable.name;
          if($copiable.copy)
          	$mode=2;
          else
          	$mode=1;
        }

		| apiid
		{$name="";$mode=0;}

	)
;

copiable returns [ String name, boolean copy, int line ]
@init { $name = ""; $copy = false; $line = 0; }
:
	(
		COPY
		{
		$copy = true;
	}

	)? ID
	{
		$name = $ID.text;
		$line = $ID.getLine();
	}

;

/*LINKS TREE*/
links
:
	ID connection ID
	(
		connection ID
	)* newlines
;

connection
:
	'->'
	| '<->'
;


/*GENERAL*/
/*singlelinechunk: ~('\r'|'\n')* (EOF|'\r'? '\n');*/

/*LEXER*/
BLOCKCOMMENT
:
	'/*' .*? '*/' -> channel ( HIDDEN )
;
/*LINECOMMENT:   '//' ~[\r\n]*  -> skip;*/
LINECOMMENT
:
	'//' ~[\r\n]*
	(
		EOF
		| '\r'? '\n'
	) -> channel ( HIDDEN )
;

WSNL
:
	(
		' '
		| '\t'
		| '\r'
	) -> channel ( HIDDEN )
;

ONEWAY
:
	'->'
;

TWOWAYS
:
	'<->'
;

FROM
:
	'from'
;

COPY
:
	'copy'
;

ADD
:
	'add'
;

SCENE
:
	'scene'
;

LBRACKET
:
	'['
;

RBRACKET
:
	']'
;

COMMA
:
	','
;

COLON
:
	':'
;

NOARGSSEMI
:
	';'
;

DEFINE
:
	'define'
;

SCHEDULE
:
	'schedule'
;

EVERY
:
	'every'
;

END
:
	'end'
;

NEWLINE
:
	'\n'
;

CARRIAGERETURN
:
	'\r'
;

STARTINGSCENE
:
	'startingScene'
;

fragment
NameChar
:
	NameStartChar
	| '0'
	| '1'
	| '2'
	| '3'
	| '4'
	| '5'
	| '6'
	| '7'
	| '8'
	| '9'
	| '_'
;

fragment
NameStartChar
:
	[a-zA-Z]
;

fragment
NameStartAPI
:
	[A-Z]
;

APIID
:
	NameStartAPI NameChar*
;

ID
:
	NameStartChar NameChar*
;

STRING
:
	'"'
	(
		~["]
		| '""'
	)* '"'
;

number
:
	unary_operator? unsigned_number
;

PLUS
:
	'+'
;

MINUS
:
	'-'
;

unary_operator
:
	'+'
	| '-'
;

unsigned_number
:
	UNSIGNED_INT
	| UNSIGNED_FLOAT
;

UNSIGNED_INT
:
	(
		'0'
		| '1' .. '9' '0' .. '9'*
	)
;

UNSIGNED_FLOAT
:
	(
		'0' .. '9'
	)+ '.'
	(
		'0' .. '9'
	)* Exponent?
	| '.'
	(
		'0' .. '9'
	)+ Exponent?
	|
	(
		'0' .. '9'
	)+ Exponent
;

fragment
Exponent
:
	(
		'e'
		| 'E'
	)
	(
		'+'
		| '-'
	)?
	(
		'0' .. '9'
	)+
;