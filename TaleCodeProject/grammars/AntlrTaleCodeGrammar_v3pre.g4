grammar AntlrTaleCodeGrammar_v3pre;

@header {
    import static alientextengine.talecode.parser.SemanticUtil.*;
    import static alientextengine.talecode.parser.Interpreter.*;
}

/*SYNTATIC*/
history
:
	{
        //Interpreter
    	startHistory();
    }
	(
		taledefinitions world newlines links*
	)
;

/*UTIL*/
newlines
:
	NEWLINE*
;

/*GLOBALS TREE*/
taledefinitions
:
	(ID ':' STRING newlines)+
;

/*Schedule actions to the tale driver*/
schedule
:
	newlines SCHEDULE (ID | defineless_define) EVERY UNSIGNED_INT newlines
;

/*define:
    newlines 'define' apiid ID newlines assignmentmap 'end';*/
/* This creates the default instance of the named object*/
define
:
	newlines 'define' definitionType ID newlines assignmentmap 'end'
;

/*Defines an item without the define keyword (used inside scenes).
This also adds the defition to the global map using the name*/
defineless_define
:
	newlines definitionType ID newlines assignmentmap 'end' newlines
;

/*Same as the defineless_define but doesn't add the definition to
the global map*/
defineless_define_noname
:
	newlines definitionType newlines assignmentmap 'end' newlines
;

assignmentmap
:
	newlines assignment*
;

/*This is a key value map used in each module of definition*/
assignment
:
	(
		ID ':' (param|params) newlines
	)
	|
	(
		ID ';' newlines
	)
;

/*Can be a default constructor call*/
apiid
:
	APIID params?
;

fromCopy
:
	FROM ID
;

definitionType
:
	fromCopy
	| apiid
;

object
:
	ID
;

params
:
	newlines '[' newlines
	(
		param
		(
			newlines ',' newlines param newlines
		)*
	)? ']'
;

param
:
	(
		copiable
		| apiid
		| STRING
		| number
	)
;

/*WORLD TREE*/
addToHistory
:
	newlines 'install' historyInsertParams
;

historyInsertParams
:
	'[' newlines addable (newlines COMMA newlines addable)* newlines ']'
;

world
:
	(define | schedule)* addToHistory? newlines scene*
;

scene
:
	'scene' ID newlines
	{
      	
        if(canDefineScene($ID.text)){
            //Interpreter
            //Register this scene and set it as the current
            registerScene($ID.text);
        }
      }

	(
		scenebody newlines
		{
	      	//Semantic
	        if(canDefineScene($ID.text)){
	            defineScene($ID.text, 
	            "scene "+$ID.text+" "+$scenebody.text+" end");
	        }else{
	            error("Scene name already used: "+$ID.text, $ID.getLine());
	        }      	
      	}

	)? 'end' newlines
;

scenebody
:
	startingscene? assignmentmap insertions? links*
;

startingscene
:
	STARTINGSCENE
	{
        if(!isStartingSceneSet()){
            setStartingScene($STARTINGSCENE.text);
            
            //Interpreter
            //Register the current active scene as the starting
            registerStartingScene();
        }else{
            error("Starting scene was already set!", $STARTINGSCENE.getLine());
        }
      }

;

insertions
:
	'add' insertparams
;

insertparams
:
	'[' newlines addable
	(
		newlines ',' newlines addable
	)* newlines ']'
;

addable
:
	(
		defineless_define
		| defineless_define_noname
		| copiable
		| apiid
	)
;

copiable
:
	COPY? ID
;

/*LINKS TREE*/
links @init { String lastScene = ""; }
:
	ID
	{
            if(!isSceneDefined($ID.text)){
                error("Can't link undefined scene! Scene: "+$ID.text, $ID.getLine());
            }
            
            //Interpreter
            lastScene = $ID.text.trim();
        }

	connection ID
	{
            if(!isSceneDefined($ID.text)){
                error("Can't link to undefined scene! Scene: "+$ID.text, $ID.getLine());
            }
            
            //Interpreter
            //Link
            link(lastScene, $ID.text.trim(), $connection.linkType);
            //Set the new last scene
            lastScene = $ID.text.trim();
        }

	(
		connection ID
		{
            if(!isSceneDefined($ID.text)){
               error("Can't link to undefined scene! Scene: "+$ID.text, $ID.getLine());
            }
            
            //Interpreter
            //Link
            link(lastScene, $ID.text.trim(), $connection.linkType);
            //Set the new last scene
            lastScene = $ID.text.trim();
         }

	)* newlines
;

connection returns [ LinkType linkType ]
:
	'->'
	{ $linkType = LinkType.GOTO; }

	| '<->'
	{ $linkType = LinkType.PATH; }

;

/*GENERAL*/
/*singlelinechunk: ~('\r'|'\n')* (EOF|'\r'? '\n');*/

/*LEXER*/
BLOCKCOMMENT
:
	'/*' .*? '*/' -> channel ( HIDDEN )
;
/*LINECOMMENT:   '//' ~[\r\n]*  -> skip;*/
LINECOMMENT
:
	'//' ~[\r\n]*
	(
		EOF
		| '\r'? '\n'
	) -> channel ( HIDDEN )
;

WSNL
:
	(
		' '
		| '\t'
		| '\r'
	) -> channel ( HIDDEN )
;

ONEWAY
:
	'->'
;

TWOWAYS
:
	'<->'
;

FROM
:
	'from'
;

COPY
:
	'copy'
;

ADD
:
	'add'
;

SCENE
:
	'scene'
;

LBRACKET
:
	'['
;

RBRACKET
:
	']'
;

COMMA
:
	','
;

COLON
:
	':'
;

NOARGSSEMI
:
	';'
;

DEFINE
:
	'define'
;

SCHEDULE
:
	'schedule'
;

EVERY
:
	'every'
;

END
:
	'end'
;

NEWLINE
:
	'\n'
;

CARRIAGERETURN
:
	'\r'
;

STARTINGSCENE
:
	'startingScene'
;

fragment
NameChar
:
	NameStartChar
	| '0'
	| '1'
	| '2'
	| '3'
	| '4'
	| '5'
	| '6'
	| '7'
	| '8'
	| '9'
	| '_'
;

fragment
NameStartChar
:
	[a-zA-Z]
;

fragment
NameStartAPI
:
	[A-Z]
;

APIID
:
	NameStartAPI NameChar*
;

ID
:
	NameStartChar NameChar*
;

STRING
:
	'"'
	(
		~["]
		| '""'
	)* '"'
;

number
:
	unary_operator? unsigned_number
;

PLUS
:
	'+'
;

MINUS
:
	'-'
;

unary_operator
:
	'+'
	| '-'
;

unsigned_number
:
	UNSIGNED_INT
	| UNSIGNED_FLOAT
;

UNSIGNED_INT
:
	(
		'0'
		| '1' .. '9' '0' .. '9'*
	)
;

UNSIGNED_FLOAT
:
	(
		'0' .. '9'
	)+ '.'
	(
		'0' .. '9'
	)* Exponent?
	| '.'
	(
		'0' .. '9'
	)+ Exponent?
	|
	(
		'0' .. '9'
	)+ Exponent
;

fragment
Exponent
:
	(
		'e'
		| 'E'
	)
	(
		'+'
		| '-'
	)?
	(
		'0' .. '9'
	)+
;