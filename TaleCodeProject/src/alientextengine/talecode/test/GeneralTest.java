package alientextengine.talecode.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.swing.JOptionPane;

import alientextengine.talecode.parser.TaleCodeParser;

public class GeneralTest {

	public static void main(String... strings) {
		String fileContent = "";
		try {

			/*
			 * fileContent = new String(Files.readAllBytes(Paths.get(
			 * "test_cases",
			 * "talecode_define_named_anywhere.tale")));
			 */

			// fileContent = new String(Files.readAllBytes(Paths.get("test_cases", "CaioUrso.tale")));
			// fileContent = new String(Files.readAllBytes(Paths.get("test_cases", "talecode_example.txt")));
			fileContent = new String(Files.readAllBytes(Paths.get(
					"test_cases/semantic",
					"talecode_undefined_scene_linking.tale")));

		} catch (IOException e) {
			e.printStackTrace();
		}

		if (fileContent != null && fileContent.length() > 0 && fileContent.contains("tale")) {
			try {
				TaleCodeParser.parseAndPlay(fileContent);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(
						null,
						e1.getMessage(),
						"Compilation error",
						JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			} finally {
				TaleCodeParser.reset();
			}
		} else {
			System.err.println("File is not a tale!");
		}
	}

}
