package alientextengine.talecode.test;

import java.awt.BorderLayout;
import java.io.FileWriter;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import alientextengine.talecode.parser.TaleCodeParser;

public class TalePad {

	// The main frame
	static JFrame		compilerFrame	= new JFrame("Tale Pad");
	// Components
	static JTextPane	codeArea		= new JTextPane();
	static JButton		compileButton	= new JButton("Compile");

	// Menu
	static JMenuBar		menuBar			= new JMenuBar();
	static JMenu		fileMenu		= new JMenu("File");

	// File menu
	static JMenuItem	openItem		= new JMenuItem("Open");
	static JMenuItem	saveItem		= new JMenuItem("Save");

	// Opening and saving
	static JFileChooser	openFileChooser	= new JFileChooser();

	public static void main(String... args) {
		// Frame arguments
		compilerFrame.setSize(600, 500);
		compilerFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		compilerFrame.setResizable(false);
		compilerFrame.setLayout(new BorderLayout());
		compilerFrame.setLocationRelativeTo(null);

		// Make the menu
		menuBar.add(fileMenu);
		fileMenu.add(openItem);
		fileMenu.add(saveItem);

		//Line number decoration
		JScrollPane scrp = new JScrollPane(codeArea);
		TextLineNumber tln = new TextLineNumber(codeArea);
		scrp.setRowHeaderView(tln);

		// Add the components
		compilerFrame.add(menuBar, BorderLayout.NORTH);
		compilerFrame.add(scrp, BorderLayout.CENTER);
		compilerFrame.add(compileButton, BorderLayout.SOUTH);

		// Show
		compilerFrame.setVisible(true);

		// Make stuff work
		openItem.addActionListener(e -> {
			int returnVal = openFileChooser.showOpenDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				try (Scanner scan = new Scanner(openFileChooser.getSelectedFile())) {
					codeArea.setText("");
					while (scan.hasNextLine()) {
						String line = scan.nextLine() + "\n";
						codeArea.setText(codeArea.getText() + line);
					}
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(
							null,
							"Couldn't open file!\n" + e1.getMessage(),
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		saveItem.addActionListener(e -> {
			int returnVal = openFileChooser.showSaveDialog(null);
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				try (FileWriter fw = new FileWriter(openFileChooser.getSelectedFile() + ".talecode")) {
					// Java 7 or above(8) automatically calls close for this writter
					fw.write(codeArea.toString());
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(
							null,
							"Couldn't save file!\n" + e1.getMessage(),
							"Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		compileButton.addActionListener(e -> {
			try {
				TaleCodeParser.parseAndPlay(codeArea.getText());
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(
						null,
						e1.getMessage(),
						"Compilation error",
						JOptionPane.ERROR_MESSAGE);
				e1.printStackTrace();
			} finally {
				TaleCodeParser.reset();
			}
		});

	}
}
