package alientextengine.talecode.test;

import javax.swing.JOptionPane;

import alientextengine.talecode.parser.TaleCodeParser;

public class GrammarTest {

	public static void main(String... strings) {
		//Pre parsing or first pass
		/*AntlrTaleCodeGrammar_v3preParser preparser = null;
		try {
			preparser = new AntlrTaleCodeGrammar_v3preParser(AntlrUtil.getTokenStream(
					TestStrings.example,
					AntlrTaleCodeGrammar_v3preLexer.class));
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			System.exit(1);
		}

		preparser.history();

		//Final parsing
		AntlrTaleCodeGrammar_v3Parser parser = null;
		try {
			parser = new AntlrTaleCodeGrammar_v3Parser(AntlrUtil.getTokenStream(
					TestStrings.example,
					AntlrTaleCodeGrammar_v3Lexer.class));
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
				| InvocationTargetException | NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			System.exit(1);
		}

		parser.history();

		if (SemanticUtil.hasErrors())
			System.out.println(SemanticUtil.errorList());
		else {
			System.out.println("Compilation sucessful.");
			TestUtil.play(Interpreter.getHistory(), "CompilerPlayer", "en_US");
		}*/

		try {
			TaleCodeParser.parseAndPlay(TestStrings.example);
		} catch (Exception e1) {
			JOptionPane.showMessageDialog(
					null,
					e1.getMessage(),
					"Compilation error",
					JOptionPane.ERROR_MESSAGE);
			e1.printStackTrace();
		} finally {
			TaleCodeParser.reset();
		}
	}

}
