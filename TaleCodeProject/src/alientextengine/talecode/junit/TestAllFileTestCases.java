package alientextengine.talecode.junit;

import static org.junit.Assert.fail;

import java.nio.file.Paths;

import org.junit.Test;

import alientextengine.talecode.error.TaleCodeException;
import alientextengine.talecode.parser.TaleCodeParser;

/**
 * Tests *failed* compilation of all error test cases.
 * Those files are located inside the test_cases folder.
 * DO NOT CHANGE THOSE FILES UNLESS YOU KNOW WHAT YOU ARE DOING.
 * 
 * @author Lucas M Carvalhaes
 *
 */
public class TestAllFileTestCases {

	/*
	 * SEMANTIC ERRORS
	 */

	@Test
	/**
	 * Tests the semantic error of copying from an uninstantiated item on scene adding
	 */
	public void t_semanticCopyNonDeclaredItemOnAddCopy() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get(
					"test_cases/semantic",
					"talecode_add_copy_of_uninstantiated_item.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 19, Object to copy isn't instantiated! ID: bottle2"))
				fail("Exception caught wrong problem. Exception Message:\n"
						+ e.getMessage()
						+ "\nExpected error was a semantic error on line 19. Copying undeclared uninstatiated item.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Linking an undeclared scene.");
	}

	@Test
	/**
	 * Tests the semantic error of copying from an uninstantiated item on definition
	 */
	public void t_semanticCopyNonDeclaredItemOnDefinition() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get(
					"test_cases/semantic",
					"talecode_copy_uninstantiated_item_on_definition.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 12, Object to copy isn't instantiated! ID: god"))
				fail("Exception caught wrong problem. Exception Message:\n"
						+ e.getMessage()
						+ "\nExpected error was a semantic error on line 12. Copying undeclared uninstatiated item.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Linking an undeclared scene.");
	}

	@Test
	/**
	 * Tests the semantic error of linking an undeclared scene.
	 */
	public void t_semanticUndeclaredSceneLinking() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths
					.get("test_cases/semantic", "talecode_undefined_scene_linking.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 53, Can't link to undefined scene! Scene: inexistingroom"))
				fail("Exception caught wrong problem. Exception Message:\n"
						+ e.getMessage()
						+ "\nExpected error was a semantic error on line 53. Linking an undeclared scene (inexistingroom).");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Linking an undeclared scene.");
	}

	@Test
	/**
	 * Tests the semantic error of adding an undeclared object to a scene.
	 */
	public void t_semanticUndeclaredObjectAdding() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get("test_cases/semantic", "talecode_undeclared_object_add.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 27, Can't add undefined object: the_thing"))
				fail("Exception caught wrong problem. Exception Message:\n" + e.getMessage()
						+ "\nExpected error was a semantic error on line 27. Adding an undeclared object (the_thing) to a scene.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Adding an undeclared object (the_thing) to a scene.");
	}

	@Test
	/**
	 * Tests the semantic error of repeated starting scene tag.
	 */
	public void t_semanticTwoStartingSceneTags() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get(
					"test_cases/semantic",
					"talecode_starting_scene_set_twice.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 31, Starting scene was already set!"))
				fail("Exception caught wrong problem. Exception Message:\n" + e.getMessage()
						+ "\nExpected error was a semantic error on line 31. Starting scene already defined.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Starting scene already defined.");
	}

	@Test
	/**
	 * Tests the semantic error of repeated ids.
	 */
	public void t_semanticRepeatedSceneIDs() {
		boolean success = true;
		try {
			TaleCodeParser
			.parseFile(Paths.get("test_cases/semantic", "talecode_scenes_repeating_names.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 30, Scene name already used: whiteroom"))
				fail("Exception caught wrong problem. Exception Message:\n" + e.getMessage()
						+ "\nExpected error was a semantic error on line 30. Scene already defined.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Scene already defined.");
	}

	@Test
	/**
	 * Tests the semantic error of repeated tale definitions.
	 */
	public void t_semanticRepeatedTaleDefinitions() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths
					.get("test_cases/semantic", "talecode_repeating_call_taledefs.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 4, Call was already registered: author."))
				fail("Exception caught wrong problem. Exception Message:\n" + e.getMessage()
						+ "\nExpected error was a semantic error on line 4. Called method twice on tale.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Called method twice on tale.");
	}

	@Test
	/**
	 * Tests the semantic error of repeated method call on scene class.
	 */
	public void t_semanticRepeatedAPIIDCallForScenes() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get(
					"test_cases/semantic",
					"talecode_repeating_call_assignment.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 27, Call was already registered: exitText."))
				fail("Exception caught wrong problem. Exception Message:\n" + e.getMessage()
						+ "\nExpected error was a semantic error on line 27. Called method twice on scene.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Called method twice on scene.");
	}

	@Test
	/**
	 * Tests the semantic error of invalid method call to apiid of scene class (no method on engine).
	 */
	public void t_semanticInvalidAPIIDCallForScenes() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get("test_cases/semantic", "talecode_invalid_scene_api.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("Key thing isn't defined for the scene api."))
				fail("Exception caught wrong problem. Exception Message:\n" + e.getMessage()
						+ "\nExpected error was a semantic error on invalid key 'thing' for the scene api.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Expected error was a semantic error on invalid key 'thing' for the scene api. Maybe someone changed the test file.");
	}

	@Test
	/**
	 * Tests the semantic error of invalid method call to apiid (no method on engine).
	 */
	public void t_semanticInvalidAPIIDCall() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get(
					"test_cases/semantic",
					"talecode_invalid_method_call_to_api.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("Key bottle isn't defined for the api id AddTriggerAction."))
				fail("Exception caught wrong problem. Exception Message:\n"
						+ e.getMessage()
						+ "\nExpected error was a semantic error on invalid key 'bottle' for AddTriggerAction.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Expected error was a semantic error on invalid key 'bottle' for AddTriggerAction. Maybe someone changed the test file.");
	}

	@Test
	/**
	 * Tests the semantic error of invalid apiid (not on engine).
	 */
	public void t_semanticInvalidAPIID() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get("test_cases/semantic", "talecode_invalid_api_id.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("Invalid API ID! No such ID in alien text engine: BananaAction"))
				fail("Exception caught wrong problem. Exception Message:\n" + e.getMessage()
						+ "\nExpected error was a semantic error. BananaAction invalid api id.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Expected error was a semantic error. BananaAction invalid api id. Maybe someone changed the test file.");
	}

	@Test
	/**
	 * Tests the semantic error of global named definitions
	 * when you define twice or more the same thing.
	 */
	public void t_semanticDefineGlobalTwice() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get("test_cases/semantic", "talecode_globals_twice_defined.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 18, Global already defined: enabletrap"))
				fail("Exception caught wrong problem. Exception Message:\n"
						+ e.getMessage()
						+ "\nExpected error was a semantic error at line 18. Enabletrap defined twice.\n Maybe someone changed the test file.");
		}
		if (success)
			fail("No exception was thrown on a semantically wrong file. Expected error was a semantic error at line 18. Enabletrap defined twice. Maybe someone changed the test file.");
	}

	/*
	 * SYNTAX ERRORS
	 */

	@Test
	/**
	 * Tests the syntax error of an APIID with lowercase first character
	 */
	public void t_syntaxLowerCaseAPIID() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get("test_cases/syntax", "talecode_lowercase_apiid.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains("At line 12, Found unexpected word: addTriggerAction."))
				fail("Exception caught wrong problem. Exception Message:\n"
						+ e.getMessage()
						+ "\nExpected error was a syntax error at addTriggerAction key. It should start with a capital letter.\n Maybe someone changed the test file.");
		}
		if (success)
			fail("No exception was thrown on a syntatically wrong file. Expected error was a syntax error at addTriggerAction key. It should start with a capital letter.\n Maybe someone changed the test file.");
	}

	@Test
	/**
	 * Tests the syntax error of a define call after the start of the scene declaration zone
	 */
	public void t_syntaxDefineAfterScene() {
		boolean success = true;
		try {
			TaleCodeParser.parseFile(Paths.get("test_cases/syntax", "talecode_define_named_anywhere.tale"));
		} catch (TaleCodeException e) {
			success = false;
			if (!e.getMessage().contains(
					"At line 15, Found unexpected word: define. Possible problem: extraneous input 'define'"))
				fail("Exception caught wrong problem. Exception Message:\n"
						+ e.getMessage()
						+ "\nExpected error was a syntax error at define key. You can only define stuff before you define scenes.\n Maybe someone changed the test file.");
		}
		if (success)
			fail("No exception was thrown on a syntatically wrong file. Expected error was a syntax error at define key. You can only define stuff before you define scenes.\n Maybe someone changed the test file.");
	}

}
