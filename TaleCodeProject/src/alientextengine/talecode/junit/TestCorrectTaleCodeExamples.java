package alientextengine.talecode.junit;

import static org.junit.Assert.fail;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;

import org.junit.Test;

import alientextengine.core.history.History;
import alientextengine.talecode.error.TaleCodeException;
import alientextengine.talecode.parser.TaleCodeParser;

/**
 * This test cases evaluate correct examples (examples that compile and generate a history).
 * Usually they take longer because some tests analyse the compilation and some use cases
 * on the generated history.
 *
 * @author Lucas M Carvalhaes
 *
 */
public class TestCorrectTaleCodeExamples {

	@Test
	/**
	 * Tests all the files that should work normally.
	 * This files should generate valid History objects.
	 */
	public void t_workingExamples() {
		History<?> caioUrso = null;
		History<?> example = null;
		try {
			caioUrso = TaleCodeParser.parseFile(Paths.get("test_cases", "CaioUrso.tale"));
			example = TaleCodeParser.parseFile(Paths.get("test_cases", "talecode_example.tale"));
		} catch (TaleCodeException e) {
			fail("Exception on correct file!\n" + e.getMessage() + "\n\nExeption dump:\n" + e.toString());
		}

		// Test some specifics about each history
		if (caioUrso == null) {
			fail("A valid history failed to compile!");
		}
		if (example == null) {
			fail("A valid history failed to compile!");
		}
		if (!caioUrso.getHistoryName().contains("Caio o Urso")) {
			fail(
					"History caio urso failed to receive its name from the tale definitions. Maybe someone changed the test files?");
		}
		if (!caioUrso.getAuthor().contains("Lucas")) {
			fail(
					"History caio urso failed to receive its author from the tale definitions. Maybe someone changed the test files?");
		}
		if (!example.getHistoryName().contains("TestParseTale")) {
			fail(
					"History talecode_example failed to receive its name from the tale definitions. Maybe someone changed the test files?");
		}
		if (!example.getAuthor().contains("Zombie")) {
			fail(
					"History talecode_example failed to receive its author from the tale definitions. Maybe someone changed the test files?");
		}
	}

	@Test
	public void t_allWorkingTestCasesShouldPassCompilation() {
		Arrays.stream(new File("test_cases/correct").listFiles()).filter(f -> f.getName().endsWith(".tale"))
				.forEach(f -> {
					try {
						History<?> h = TaleCodeParser.parseFile(f.toPath());
						if (h == null) {
							fail("Compiling correct example failed");
						}
					} catch (Exception e) {
						e.printStackTrace();
						fail("Compiling correct example (" + f.getName() + ") failed: " + e.getMessage());
					}
				});
	}

}
