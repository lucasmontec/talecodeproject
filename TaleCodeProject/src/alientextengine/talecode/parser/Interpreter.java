package alientextengine.talecode.parser;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

import com.esotericsoftware.kryo.Kryo;

import alientextengine.annotation.APIHidden;
import alientextengine.core.TextCommand;
import alientextengine.core.graph.GraphHistory;
import alientextengine.core.graph.SceneNode;
import alientextengine.core.history.History;
import alientextengine.core.history.Restriction;
import alientextengine.core.processing.IStreamReader;
import alientextengine.model.action.Action;
import alientextengine.model.action.IAction;
import alientextengine.model.action.Retriever;
import alientextengine.model.general.Actor;
import alientextengine.model.general.IActor.Visibility;
import alientextengine.model.general.NPCController;
import alientextengine.model.streamreader.ActionGraphStreamReader;
import alientextengine.model.streamreader.ActionNode;
import alientextengine.model.trigger.Trigger;

/**
 * A class responsible for creating all instances required to run a {@link GraphHistory History} in the Tale Engine.
 *
 * @author Lucas M Carvalhaes
 *
 */
public class Interpreter {

	/** The history holder */
	private static GraphHistory						history;

	/** Map for global defined instances ID->object */
	private static HashMap<String, Object>			apiObjects;

	/**
	 * A stack for parameter objects. This is used to instantiate objects when
	 * instantiating other objects
	 */
	private static HashMap<String, Queue<Object>>	apiQueue;

	/** Context reference to the current scene */
	private static SceneNode						currentScene;

	/** The starting scene node to bind later */
	private static SceneNode						startingScene;

	/** Store any interpretation problems */
	private static HashMap<String, Integer>			errors;

	/**
	 * A Kryo seriualizer to copy (deep copy) objects.
	 */
	private static Kryo								kryo;

	/**
	 * The reflections object that tracks the engine
	 * This is to use later
	 */
	// private static Reflections reflections = new Reflections("alientextengine");

	/*
	 * Initialization
	 */
	static {
		apiObjects = new HashMap<>();
		apiQueue = new HashMap<>();
		errors = new HashMap<>();
		kryo = new Kryo();
	}

	public static void reset() {
		apiObjects.clear();
		apiQueue.clear();
		errors.clear();
		startingScene = null;
		currentScene = null;
		history = null;
	}

	/**
	 * Call this to create a new history instance to work with the static
	 * methods
	 */
	public static void startHistory() {
		history = new GraphHistory();
	}

	/**
	 * Create a new scene instance. Sets it to the current scene and registers
	 * the scene with its ID in the scenemap.
	 *
	 * @param sceneID
	 *            The ID to register the scene
	 */
	public static void registerScene(String sceneID) {
		currentScene = new SceneNode();
		apiObjects.put(sceneID, currentScene);
	}

	/**
	 * To be used in the post-parser before parsing the scene body
	 *
	 * @param sceneID
	 */
	public static void setCurrentScene(String sceneID) {
		currentScene = (SceneNode) apiObjects.get(sceneID);
	}

	/**
	 * Registers the currentscene as the starting scene for late binding
	 */
	public static void registerStartingScene() {
		startingScene = currentScene;
		startingScene.setHistory(history);
	}

	/**
	 * Registers to the history the tale title and tale author.
	 *
	 * @param ID
	 *            The ID being author, tale or language
	 * @param value
	 *            The value to set the id to
	 */
	public static void registerHeader(String ID, String value) {
		if (ID == null) {
			return;
		}
		if (ID.equals("author") && value != null) {
			history.setAuthor(value.replace("\"", ""));
		}
		if (ID.equals("tale") && value != null) {
			history.setHistoryName(value.replace("\"", ""));
		}
		if (ID.equals("language") && value != null) {
			history.setLanguageTag(value.replace("\"", ""));
		}
	}

	private static boolean isNumber(String s) {
		boolean dotted = false;
		if (s.isEmpty()) {
			return false;
		}
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1) {
					return false;
				} else {
					continue;
				}
			}
			if (Character.digit(s.charAt(i), 10) < 0) {
				if (s.charAt(i) == '.' && !dotted) {
					dotted = true;
					continue;
				} else {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * This method gets the parameter list as an string and creates a object
	 * array list from the parameters finding each parameter and casting to the
	 * correct class or retrieving the object.
	 *
	 * @param parameterList
	 *            The parameter list as text
	 * @return The array containing the correct object parameters
	 */
	public static ArrayList<Object> makeParameterList(ArrayList<String> parameterList) {
		ArrayList<Object> parameters = new ArrayList<>();
		for (String paramname : parameterList) {
			// Find the type of parameter - Object / Integer / String
			if (paramname.contains("\"") && !paramname.contains("[")) {// String
				parameters.add(paramname.replace("[", "").replace("]", "").replace("\"", ""));
			} else if (isNumber(paramname.replace("[", "").replace("]", ""))) {// Number
				if (paramname.contains(".") || paramname.contains("f")) {
					parameters.add(Float.parseFloat(paramname.replace("[", "").replace("]", "")));
				} else {
					parameters.add(Integer.parseInt(paramname.replace("[", "").replace("]", "")));
				}
			} else {// Object or anonymous instance
				int first = paramname.indexOf("[");
				if (first > 0) {// Is dynamic instance
					String tp = paramname.substring(0, first).trim();
					parameters.add(getFirstOfTypeInApiQueue(tp));
				} else // Is an object reference or boolean
				if (paramname.equals("true") || paramname.equals("false")) {
					parameters.add(Boolean.parseBoolean(paramname));
				} else
				// is an object (make copy?)
				if (paramname.contains("copy")) {
					parameters.add(kryo.copy(apiObjects.get(paramname.replace("copy", "").trim())));
				} else {
					parameters.add(apiObjects.get(paramname));
				}
			}
		}
		// System.out.println("list: "+parameterList);
		// System.out.println("generated: "+parameters);
		return parameters;
	}

	private static Object getFirstOfTypeInApiQueue(String type) {
		if (apiQueue.get(type) == null) {
			return null;
		}
		return apiQueue.get(type).poll();
	}

	private static void storeInApiQueue(String ID, Object obj) {
		if (apiQueue.get(ID) == null) {
			apiQueue.put(ID, new LinkedBlockingQueue<Object>());
		}

		apiQueue.get(ID).offer(obj);
	}

	/**
	 * Creates the instance and adds it to the object stack
	 *
	 * @param ID
	 *            The ID of the class to instantiate
	 * @param parameterList
	 *            The parameter list
	 */
	public static void apiInstantiateQueue(String ID, ArrayList<String> parameterList, int line) {
		if (SemanticUtil.hasErrors()) {
			errors.put(
					"Couldn't create instance of: " + ID + "\nThere are semantic errors. Semantic dump:\n"
							+ SemanticUtil.errorList(),
					-1);
			return;
		}
		Object o = createAPIInstance(ID, parameterList, line);
		if (o == null) {
			errors.put("Couldn't instantiate the object: " + ID + parameterList, line);
			return;
		}
		storeInApiQueue(ID, o);
	}

	/**
	 * Copies the instance and adds it to the object stack
	 *
	 * @param ID
	 *            The ID of the class to instantiate
	 * @param parameterList
	 *            The parameter list
	 */
	public static void apiCopyQueue(String ID, int line) {
		if (SemanticUtil.hasErrors()) {
			errors.put(
					"Couldn't create copy of object: " + ID + "\nThere are semantic errors. Semantic dump:\n"
							+ SemanticUtil.errorList(),
					line);
			return;
		}
		Object o = kryo.copy(apiObjects.get(ID));
		if (o == null) {
			errors.put("Couldn't copy the object: " + ID, line);
			return;
		}
		storeInApiQueue(ID, o);
	}

	/**
	 * Create the API class instance registering it to the object map
	 *
	 * @param ID
	 *            The class ID to register in the obj map
	 * @param parameterList
	 *            The parameter list to use
	 */
	private static Object createAPIInstance(String ID, ArrayList<String> parameterList, int line) {

		// System.out.println("Creating API Instance for: " + ID);
		// System.out.println("Paremeter list: " + parameterList);

		Class<?> cla = SemanticUtil.getAPIClass(ID);
		// Don't accept invalid api ids
		if (cla == null) {
			return null;
		}

		Constructor<?> ctor = null;
		Constructor<?>[] ctors = cla.getDeclaredConstructors();
		// Find the correct constructor
		if (parameterList == null) {
			// System.out.println("Creating with default constructor.");
			// Create instance of the object and register to the object map
			try {
				return cla.newInstance();
			} catch (InstantiationException | IllegalAccessException e) {
				// e.printStackTrace();
				errors.put(
						"Constructor not found for class: " + cla.getName() + "\nError: " + e.toString(),
						line);
			}
		} else {
			// System.out.println("Creating with parameter constructor.");
			// Create the parameter array
			ArrayList<Object> params = makeParameterList(parameterList);// make
			// param
			// list
			boolean ok = true;
			int j = 0;
			// Match lengths
			for (int i = 0; i < ctors.length; i++) {
				// System.out.println("checking: " + Arrays.toString(ctors[i].getGenericParameterTypes()));
				ok = true;
				// Match types
				if (ctors[i].getGenericParameterTypes().length == params.size()) {
					j = 0;
					for (Class<?> c : ctors[i].getParameterTypes()) {
						/*
						 * System.out.println(
						 * "comparing " + c.getName() + " with "
						 * + (params.get(j) == null ? "null" : params.get(j).getClass()));
						 */
						if (params.get(j) == null || !ClassUtils.areRelated(c, params.get(j).getClass())) {
							// System.out.println("invalid");
							ok = false;
						} /*
							 * else
							 * System.out.println("OK!");
							 */
						j++;
					}
					if (ok) {

						// System.out.println("Found: " + ctors[i].toGenericString());

						ctor = ctors[i];
						break;
					}
				}
			}

			// Find vararg ctor

			ok = true;
			boolean isVaArg = false;
			Object vararg_param = null;
			List<Object> params_with_varargs = null;
			// Scan for vararg
			if (ctor == null) {
				// System.out.println("Trying with vargs");
				Class<?> varargClass = null;
				for (int i = 0; i < ctors.length; i++) {
					ok = true;
					// j marches the parameter list to analyse parameters
					// reset j
					j = 0;
					// Find ctors that have varargs
					if (Arrays.toString(ctors[i].getGenericParameterTypes()).contains("Ljava")) {
						// System.out.println("Varg ctor: " + ctors[i].toGenericString());
						// Match parameters until the vararg
						for (Class<?> c : ctors[i].getParameterTypes()) {
							// stop when find an array (vararg)
							if (c.isArray()) {
								// System.out.println("Found the varg type: " + c.getName() + " at j=" + j);
								varargClass = c;
								j++;
								break;
							}
							if (params.get(j) == null
									|| !ClassUtils.areRelated(c, params.get(j).getClass())) {

								/*
								 * System.out.println(
								 * "Normal parameter failed: " + c.getName() + " of type "
								 * + c.getTypeName() + "\nIt should be: "
								 * + params.get(j).getClass());
								 */

								ok = false;
							}
							j++;
						}
						// Check if we can make the vararg list obj
						try {
							vararg_param = params.subList(j - 1, params.size()).toArray();
						} catch (Exception ce) {
							ok = false;
							if (!(ce instanceof IndexOutOfBoundsException)) {
								System.err.println(
										"Couldn't cast "
												+ params.subList(j - 1, params.size()).toArray().getClass()
														.getName()
												+ " to vararg class " + varargClass.getName() + ".");
							} else {
								System.err.println("Index is out of bounds!");
							}
						}

						params_with_varargs = new ArrayList<>();
						if (j - 1 >= 0) {
							List<Object> prev_params = params.subList(0, j - 1);
							if (prev_params != null && !prev_params.isEmpty()) {
								params_with_varargs.addAll(prev_params);
							}
						}
						params_with_varargs.add(vararg_param);

						// Found a valid vararg ctor and made the obj
						if (ok && varargClass.equals(Object[].class)) {
							ctor = ctors[i];

							/*
							 * System.out.println(
							 * "found varg constructor: " + ctor.getParameterCount() + " params for: "
							 * + params_with_varargs.size() + " created params.");
							 * System.out.println(params_with_varargs);
							 */

							isVaArg = true;
							break;
						} /*
							 * else {
							 * errors.add("Incompatible var arg constructor. API defines a vararg that is different from 'Object...'");
							 * }
							 */
					}
				}
			}

			// Create instance of the object and register to the object map
			if (ctor != null) {
				try {
					if (isVaArg && params_with_varargs != null) {
						return ctor.newInstance(params_with_varargs.toArray());
					} else {
						return ctor.newInstance(params.toArray());
					}
				} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					e.printStackTrace();
					errors.put("Instantiation failure! Error:" + e.getMessage(), line);
				}
			} else {
				errors.put("No constructor found!", line);
			}
		}

		return null;
	}

	/**
	 * Adds the queue object to the object map with the ID
	 *
	 * @param objectID
	 */
	public static void registerObject(String objectID, String type) {
		// System.out.println("registering: " + objectID);
		Object o = getFirstAPIObjectInQueueAvaliable(type);
		// System.out.println("registering [" + objectID + "]: " + o);
		if (o == null) {
			return;
		}
		apiObjects.put(objectID, o);
	}

	/**
	 * Adds an object to the current scene using the object ID
	 * previously declared on the TaleCode file
	 *
	 * @param objectID
	 *            The object ID to add
	 */
	public static void addObjectByName(String objectID) {
		// System.out.println(objectID);
		Object o = apiObjects.get(objectID);
		if (o == null) {
			return;
		}
		// Add
		addAny(o);
	}

	/**
	 * Adds a copy of object to the current scene using the object ID
	 * previously declared on the TaleCode file
	 *
	 * @param objectID
	 *            The object ID to copy and add
	 */
	public static void addObjectCopyByName(String objectID) {
		Object o = apiObjects.get(objectID);
		if (o == null) {
			return;
		}
		// Deep copy o
		Object copy = kryo.copy(o);
		// Add the deep copied object
		addAny(copy);
	}

	private static void addAny(Object o) {
		if (currentScene == null) {
			System.err.println("Current scene is NULL!");
			return;
		}
		// Add as actor
		if (Actor.class.isAssignableFrom(o.getClass())) {
			currentScene.addActor((Actor) o);
		}
		// Add as trigger
		if (Trigger.class.isAssignableFrom(o.getClass())) {
			currentScene.addTrigger((Trigger) o);
		}
		// Add as restriction
		if (Restriction.class.isAssignableFrom(o.getClass())) {
			currentScene.addRestriction((Restriction) o);
		}
		// If is a action node, add to the history and bind to the scene
		// The default behavior is to add with a master, non-hungry and bound to the scene
		if (ActionNode.class.isAssignableFrom(o.getClass())) {
			ActionGraphStreamReader reader = new ActionGraphStreamReader(new ActionNode((ActionNode) o),
					currentScene, false);
			history.addStreamReaderToInstall(reader);
		}
		// If is an action graph stream reader, install it to the history
		if (ActionGraphStreamReader.class.isAssignableFrom(o.getClass())) {
			history.addStreamReaderToInstall((IStreamReader) o);
		}
		// Install NPCs too, to history and scene
		if (NPCController.class.isAssignableFrom(o.getClass())) {
			((NPCController) o).install(history, currentScene);
		}
	}

	/**
	 * Add the first object avaliable in the api object queue to the current
	 * scene
	 */
	public static void addObjectByQueue() {
		Object o = getFirstAPIObjectInQueueAvaliable();
		if (o == null) {
			return;
		}
		// Add
		addAny(o);
	}

	private static Object getFirstAPIObjectInQueueAvaliable() {
		for (Queue<Object> q : apiQueue.values()) {
			if (!q.isEmpty()) {
				return q.poll();
			}
		}

		return null;
	}

	private static Object getFirstAPIObjectInQueueAvaliable(String type) {
		/*
		 * for (Queue<Object> q : apiQueue.values()) {
		 * if (!q.isEmpty())
		 * return q.poll();
		 * }
		 */
		Object o = null;
		if (apiQueue.get(type) != null) {
			o = apiQueue.get(type).poll();
		}
		if (o == null) {
			return getFirstAPIObjectInQueueAvaliable();
		}

		return o;
	}

	/**
	 * Call this inside a scene body to create a call to the API method of the
	 * scene. This is used to set scene parameters after construction.
	 *
	 * @param methodKey
	 *            The method key or name
	 * @param methodValue
	 *            The method value parsed as string
	 */
	public static void sceneApiCall(String methodKey, ArrayList<String> methodValues) {
		if (methodKey == null || methodKey.length() == 0) {
			return;
		}
		// Preprocess the method name
		String methodKeyName = "set" + methodKey.toLowerCase();
		// Look through all methods
		Method method = Arrays.stream(SceneNode.class.getMethods()).filter(
				m -> m.getName().toLowerCase().equals(methodKeyName)
						|| m.getName().toLowerCase().contains(methodKeyName))
				.findFirst().get();

		// Only one val
		String methodValue = methodValues.get(0);

		// Method value - crude: params can be apiids and objects too
		Integer methodVal = null;
		if (!methodValue.contains("\"")) {
			methodVal = Integer.parseInt(methodValue);
		}

		// If we have a valid method
		if (method != null) {
			try {
				if (methodVal == null) {
					method.invoke(currentScene, methodValue.replace("\"", ""));
				} else {
					method.invoke(currentScene, methodVal);
				}
			} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Call this inside a definition body to create a call to the API method of
	 * the API class. This is used to set api parameters after construction.
	 *
	 * In the define call, the defined object is the definitionname.
	 *
	 * @param definitionname
	 *            The name of the global apiobjects map of the defined object
	 * @param methodKey
	 *            The method key or name
	 * @param methodValue
	 *            The method value parsed as string
	 * @param line
	 *            The line of this call
	 */
	public static void definitionApiCallByName(String definitionname, String methodKey,
			ArrayList<String> methodValues, int line) {
		// System.out.println(definitionname);
		// Get the object to receive the calls
		Object definedObject = apiObjects.get(definitionname);

		if (definedObject == null) {
			errors.put(
					"Invalid object on a define call. Cannot do method calls. Definition name: "
							+ definitionname,
					line);
			return;
		}

		// System.out.println(definedObject.getClass().getSimpleName());
		definitionApiCall(definedObject, methodKey, methodValues, line);
	}

	public static void definitionApiCallByTypeCurrent(String type, String methodKey,
			ArrayList<String> methodValues, int line) {
		// Strip the parameters that come with the type
		type = type.substring(0, type.indexOf("["));
		// Get the object to receive the calls
		// System.out.println(apiQueue + "\n" + type);
		Object definedObject = apiQueue.get(type).peek();

		if (definedObject == null) {
			errors.put("Invalid object on definition post method call.", line);
			return;
		}

		// System.out.println(definedObject.getClass().getSimpleName());
		definitionApiCall(definedObject, methodKey, methodValues, line);
	}

	/**
	 * Call this inside a definition body to create a call to the API method of
	 * the API class. This is used to set api parameters after construction.
	 *
	 * @param definedObject
	 *            the object to receive the method calls
	 * @param methodKey
	 *            The method key or name
	 * @param methodValues
	 *            The method value parsed as string
	 */
	public static void definitionApiCall(Object definedObject, String methodKey,
			ArrayList<String> methodValues, int line) {
		if (methodKey == null || methodKey.length() == 0) {
			return;
		}

		// Get the class
		Class<?> cla = definedObject.getClass();

		// First find methods that are exactly what is written
		// Look through all methods
		List<Method> methods = Arrays.stream(cla.getMethods())
				.filter(m -> m.getName().toLowerCase().equals(methodKey.toLowerCase()))
				.filter(m -> m.getAnnotation(APIHidden.class) == null).collect(Collectors.toList());

		// Now get all methods with set
		// Preprocess the method name
		String methodKeyProcessed = "set" + methodKey.toLowerCase();

		// Look through all methods
		methods.addAll(Arrays.stream(cla.getMethods()).filter(
				m -> m.getName().toLowerCase().equals(methodKeyProcessed)
						|| m.getName().toLowerCase().contains(methodKeyProcessed))
				.filter(m -> m.getAnnotation(APIHidden.class) == null).collect(Collectors.toList()));

		/*
		 * System.out.println("\n\n***Method: " + methodKey + " Class: " + cla.getName());
		 * methods.stream().forEach(m -> {
		 * System.out.println(m.getName());
		 * });
		 */

		// Find methods that aren't sets
		if (methods.size() == 0) {
			methods.addAll(Arrays.stream(cla.getMethods()).filter(
					m -> m.getName().toLowerCase().equals(methodKey.toLowerCase())
							|| m.getName().toLowerCase().contains(methodKey.toLowerCase()))
					.filter(m -> m.getAnnotation(APIHidden.class) == null).collect(Collectors.toList()));
		}

		if (methods.size() == 0) {
			errors.put(
					"Invalid method: " + methodKey + " for " + cla.getSimpleName()
							+ " on definition method call.",
					line);
			return;
		}

		// Invoke without value
		if (methods.size() > 0 && methodValues == null) {
			boolean failed = true;
			int currentMethod = 0;
			HashMap<String, Integer> possibleErrors = new HashMap<>();
			while (failed && currentMethod < methods.size()) {
				try {
					// System.out.println("Invoking: " + method.getName());
					methods.get(currentMethod).invoke(definedObject, new Object[] {});
					failed = false;
				} catch (IllegalAccessException | InvocationTargetException e) {
					possibleErrors.put(
							"Possible problem: Exception of access or invocation target. Exception text: "
									+ e.getLocalizedMessage(),
							line);
					// e.printStackTrace();
				} catch (IllegalArgumentException iae) {
					possibleErrors.put(
							"Possible problem: Illegal argument for no arguments method. Exception text: "
									+ iae.getLocalizedMessage(),
							line);
					// iae.printStackTrace();
				}
				// Advance to next method
				currentMethod++;
			}
			if (failed) {
				errors.putAll(possibleErrors);
			}
			return;
		}

		// The method values array for processing string values into other values
		ArrayList<Object> processedValues = new ArrayList<>();

		// Process each value int a valid object
		for (String methodValue : methodValues) {
			// Get the method value parameter
			Float numberValue = null;
			Object objValue = null;
			boolean invalid = false;
			boolean realString = false;
			// Find out what the value is
			if (methodValue.contains("\"") && !methodValue.contains("[")) {
				// Is a string
				realString = true;
			} else if (isNumber(methodValue)) {
				// Is a number
				numberValue = Float.parseFloat(methodValue);
			} else if (methodValue.contains("[")) {
				// Is a constructor
				// Get the ID beeing constructed
				String ID = methodValue.substring(0, methodValue.indexOf("["));
				if (apiQueue.get(ID) == null) {
					errors.put("Undefined object reference: " + ID + ".", line);
					return;
				}
				objValue = apiQueue.get(ID).poll();
			} else if (methodValue.equals("true") || methodValue.equals("false")) {
				// Is a boolean value
				objValue = Boolean.parseBoolean(methodValue);
			} else if (methodValue.equals("visible") || methodValue.equals("unlisted")
					|| methodValue.equals("invisible")) {
				// It is a visibility enum
				objValue = Visibility.valueOf(methodValue.toUpperCase());
			} else if (methodValue.equals("self")) {
				// Self reference
				objValue = definedObject; // Refer to itself.
			} else {
				// is an object reference or copy
				if (methodValue.contains("copy")) {
					objValue = kryo.copy(apiObjects.get(methodValue.replace("copy", "").trim()));
				} else {
					objValue = apiObjects.get(methodValue);
				}

				if (objValue == null) {
					invalid = true;
					errors.put("Undefined object reference: " + methodValue + ".", line);
				}
			}

			// Store the processed object
			if (numberValue == null) {
				if (objValue == null) {// Both null means string
					if (realString) {
						processedValues.add(methodValue.replace("\"", ""));
					} else {
						errors.put("Undefined object reference: " + methodValues + ".", line);
					}
				} else {// This means object ref or constructor or boolean
					if (!invalid) {
						processedValues.add(objValue);
					}
				}
			} else {// This means number
				processedValues.add(numberValue);
			}
		}

		// If we have a valid method
		if (methods.size() > 0) {
			boolean failed = true;
			int currentMethod = 0;
			HashMap<String, Integer> possibleErrors = new HashMap<>();
			while (failed && currentMethod < methods.size()) {
				ArrayList<Object> processedValuesCpy = new ArrayList<>(processedValues);

				try {
					boolean wasRetriever = false;
					// If is a single param, check retriever before call
					if (processedValuesCpy.size() == 1) {
						// If we can install the retriever, we do it
						if (processedValuesCpy.get(0) instanceof Retriever
								&& definedObject instanceof Action) {
							((Retriever) processedValuesCpy.get(0))
									.setLoadTarget(methods.get(currentMethod), definedObject);
							((Action) definedObject).registerRetriever((Retriever) processedValuesCpy.get(0));
							wasRetriever = true;
						}
					}

					// Now we can conform the parameters
					Method current = methods.get(currentMethod);
					if (!wasRetriever) {
						Class<?>[] paramTypes = current.getParameterTypes();
						boolean paramsOk = true;
						if (paramTypes.length == processedValuesCpy.size()) {
							for (int i = 0; i < current.getParameterCount(); i++) {
								// Treat int to float conversion
								if (paramTypes[i].equals(int.class) || paramTypes[i].equals(Integer.class)) {
									processedValuesCpy.set(i, Math.round((Float) processedValuesCpy.get(i)));
								}

								// Check is params match
								boolean notEqualOrAssigneableClasses = false;

								if (ClassUtils.isWrapperType(paramTypes[i])) {
									notEqualOrAssigneableClasses = !processedValuesCpy.get(i).getClass()
											.equals(paramTypes[i])
											&& !paramTypes[i]
													.isAssignableFrom(processedValuesCpy.get(i).getClass());
								}

								boolean primitivesDontMatch = false;

								// If the param type is a primitive
								if (ClassUtils.isPrimitiveType(paramTypes[i])) {
									try {
										primitivesDontMatch = !paramTypes[i].equals(
												processedValuesCpy.get(i).getClass().getField("TYPE")
														.get(null));
									} catch (NoSuchFieldException | SecurityException e) {}
								}

								if (notEqualOrAssigneableClasses || primitivesDontMatch) {
									possibleErrors.put(
											"Argument type mismatch in method call. Expecting "
													+ paramTypes[i].getSimpleName() + " but got "
													+ processedValuesCpy.get(i).getClass().getSimpleName()
													+ " \nMethod:" + current + "\n",
											line);
									paramsOk = false;
								}
							}
						} else {
							possibleErrors.put(
									"Wrong number of arguments in method call. Expecting " + paramTypes.length
											+ " but got " + processedValuesCpy.size() + " arguments.\nMethod:"
											+ current + "\n",
									line);
							paramsOk = false;
						}

						// Lets try to invoke now
						if (paramsOk) {
							current.invoke(definedObject, processedValuesCpy.toArray());
							failed = false;
						}
					}

				} catch (IllegalAccessException | InvocationTargetException e) {
					possibleErrors.put(
							"Exception of access or invocation target. Exception text: "
									+ e.getLocalizedMessage(),
							line);
					// e.printStackTrace();
				} catch (IllegalArgumentException e) {
					possibleErrors.put(
							"Argument exception for method." + ".\nMethod: "
									+ methods.get(currentMethod).getName() + "\nMessage: " + e.getMessage()
									+ "\n",
							line);
					/*
					 * System.err.println(
					 * "Argument type mismatch in method call for definition." + "\nMethod: "
					 * + methods.get(currentMethod).getName());
					 */
					// e.printStackTrace();
				}
				// Advance to next method
				currentMethod++;
			}
			if (failed) {
				errors.putAll(possibleErrors);
			}
			return;
		}
	}

	// Path is bidirectional, goto is left to right
	public enum LinkType {
		PATH,
		GOTO
	};

	/**
	 * Use this method to link scenes together to form the history graph. If the
	 * type is goes to this will link scene A with a one directional way to
	 * scene B. If the link is of type Path to this will bind the scenes with a
	 * bidirectional path between them.
	 * Won't do a thing if one of the scenes isn't defined properly (null on apiObjects).
	 *
	 * @param sceneA
	 *            The scene A ID (can't be null)
	 * @param sceneB
	 *            The scene B ID (can't be null)
	 * @param type
	 *            The {@link LinkType type} of link to use
	 */
	public static void link(String sceneA, String sceneB, LinkType type) {
		if (apiObjects.get(sceneA) == null || apiObjects.get(sceneB) == null) {
			return;
		}
		switch (type) {
			case GOTO:
				((SceneNode) apiObjects.get(sceneA)).goesTo((SceneNode) apiObjects.get(sceneB));
				break;
			case PATH:
				((SceneNode) apiObjects.get(sceneA)).pathTo((SceneNode) apiObjects.get(sceneB));
				break;
			default:
				break;
		}
	}

	/**
	 * Adds an object to the history by its ID.
	 * Only instantiated object
	 *
	 * @param objectID
	 *            The object ID to look for and add
	 */
	public static void addAnyToHistoryByName(String objectID, int line) {
		if (!apiObjects.containsKey(objectID)) {
			errors.put("Undeclared object on install to history: " + objectID, line);
			return;
		}
		if (Trigger.class.isAssignableFrom(apiObjects.get(objectID).getClass())) {
			history.addTrigger((Trigger) apiObjects.get(objectID));
			return;
		}
		if (TextCommand.class.isAssignableFrom(apiObjects.get(objectID).getClass())) {
			history.addCommand((TextCommand) apiObjects.get(objectID));
			return;
		}
		if (IStreamReader.class.isAssignableFrom(apiObjects.get(objectID).getClass())) {
			history.addStreamReaderToInstall((IStreamReader) apiObjects.get(objectID));
			return;
		}
	}

	/**
	 * Adds a scheduled action to the history (to install later on the tale driver).
	 *
	 * @param objectID
	 *            The ID of the object of the action
	 * @param interval
	 *            The interval in Miliseconds to run the action
	 * @param line
	 *            The line of code adding the action
	 */
	public static void scheduleToTaleDriver(String objectID, String interval, int line) {
		if (!apiObjects.containsKey(objectID)) {
			errors.put("Undeclared object on schedule: " + objectID, line);
			return;
		}

		Object possibleAction = apiObjects.get(objectID);
		if (!(possibleAction instanceof IAction)) {
			errors.put("Only actions can be scheduled. " + objectID + " is not an action.", line);
			return;
		}

		// Default value shouldn't ever pass
		int interv = 100;

		// This shouldn't fail (the syntax fails first always)
		try {
			interv = Integer.parseInt(interval);
		} catch (NumberFormatException nfe) {
			errors.put("Interval must be a number (positive integer). " + objectID + " failed.", line);
			return;
		}

		// Install the schedule action
		IAction action = (IAction) possibleAction;
		history.addActionToSchedule(action, interv);
	}

	/**
	 * Returns true if there is an object with this ID instantiated on the apiObjects map.
	 *
	 * @param ID
	 *            The ID to look for
	 * @return true if it is instantiated
	 */
	public static boolean isObjectInstantiated(String ID) {
		return apiObjects.containsKey(ID);
	}

	/**
	 * Returns the simple class name (APIID) from the object.
	 *
	 * @param ID
	 *            The object to look for
	 * @return The object APIID (Simple class name)
	 */
	public static String getApiIDFromDeclaredObject(String ID) {
		return apiObjects.get(ID).getClass().getSimpleName();
	}

	/**
	 * Binds all scenes and adds what is needed to the starting scene
	 */
	public static void endHistory() {
		// Bind the starting scene to the history
		history.setInitialNode(startingScene);
	}

	/**
	 * @return The current history
	 */
	public static History<?> getHistory() {
		return history;
	}

	/**
	 * @return True if any interpretation errors occurred
	 */
	public static boolean hasErrors() {
		return errors.size() > 0;
	}

	/**
	 * @return The errors concatenated as {@link ArrayList#toString()}.
	 */
	public static String errorList() {
		return errors.toString();
	}

	public static HashMap<String, Integer> errors() {
		return errors;
	}
}
