package alientextengine.talecode.parser;

import java.util.HashSet;
import java.util.Set;

public class ClassUtils {

	private static final Set<Class<?>> WRAPPER_TYPES = getWrapperTypes();
	private static final Set<Class<?>>	PRIMITIVE_TYPES	= getPrimitiveTypes();

	public static boolean isWrapperType(Class<?> clazz) {
		return WRAPPER_TYPES.contains(clazz);
	}

	/**
	 * This returs true if c1 can be passed to c2 by some mean of
	 * inheritance.
	 * Basically this returns true if c1 is the same class of c2,
	 * or c1 inherits from c2, or c1 is tied to c2 by auto boxing.
	 * 
	 * @return as said before
	 */
	public static boolean areRelated(Class<?> c1, Class<?> c2) {
		if (c2.isAssignableFrom(c1))
			return true;
		if (c1.isAssignableFrom(c2))
			return true;
		if (c1.equals(c2))
			return true;
		if (c1 == c2)
			return true;
		if (areTiedByAutoboxing(c1, c2))
			return true;

		return false;
	}

	/**
	 * This method checks if either class is a wrapper and if it wraps the other.
	 * 
	 * @param c1
	 *            A wrapper or primitive
	 * @param c2
	 *            A wrapper or primitive
	 * @return True if one of the classes wraps the other
	 */
	public static boolean areTiedByAutoboxing(Class<?> c1, Class<?> c2) {
		boolean primitivesMatch = false;

		if (isPrimitiveType(c1) && isWrapperType(c2))
			try {
				primitivesMatch = c1.equals(c2.getField("TYPE").get(null));
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException
					| IllegalAccessException e) {}

		if (isPrimitiveType(c2) && isWrapperType(c1))
			try {
				primitivesMatch = c2.equals(c1.getField("TYPE").get(null));
			} catch (NoSuchFieldException | SecurityException | IllegalArgumentException
					| IllegalAccessException e) {}

		return primitivesMatch;
	}

	private static Set<Class<?>> getPrimitiveTypes() {
		Set<Class<?>> ret = new HashSet<Class<?>>();
		ret.add(boolean.class);
		ret.add(char.class);
		ret.add(byte.class);
		ret.add(short.class);
		ret.add(int.class);
		ret.add(long.class);
		ret.add(float.class);
		ret.add(double.class);
		ret.add(void.class);
		return ret;
	}

	private static Set<Class<?>> getWrapperTypes() {
		Set<Class<?>> ret = new HashSet<Class<?>>();
		ret.add(Boolean.class);
		ret.add(Character.class);
		ret.add(Byte.class);
		ret.add(Short.class);
		ret.add(Integer.class);
		ret.add(Long.class);
		ret.add(Float.class);
		ret.add(Double.class);
		ret.add(Void.class);
		return ret;
	}

	public static boolean isPrimitiveType(Class<?> clazz) {
		return PRIMITIVE_TYPES.contains(clazz);
	}

}
