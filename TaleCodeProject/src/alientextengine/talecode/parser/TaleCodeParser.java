package alientextengine.talecode.parser;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;

import javax.swing.JOptionPane;

import alientextengine.core.history.History;
import alientextengine.talecode.antlr.AntlrTaleCodeGrammar_v3Lexer;
import alientextengine.talecode.antlr.AntlrTaleCodeGrammar_v3Parser;
import alientextengine.talecode.antlr.AntlrTaleCodeGrammar_v3preLexer;
import alientextengine.talecode.antlr.AntlrTaleCodeGrammar_v3preParser;
import alientextengine.talecode.error.TaleCodeErrorReport;
import alientextengine.talecode.error.TaleCodeException;
import alientextengine.util.TestUtil;

public class TaleCodeParser {

	/**
	 * Parses a TaleCode file and generates a history from it.
	 * 
	 * @param path
	 *            The path to the file to generate.
	 * @return
	 * @throws TaleCodeException
	 */
	public static History<?> parseFile(Path path) throws TaleCodeException {
		return parseFile(path, false);
	}

	/**
	 * Parses a TaleCode file and generates a history from it.
	 * 
	 * @param path
	 *            The path to the file to generate.
	 * @param antlrConsoleReport
	 *            set to true to spit all errors that Antlr generates to console.
	 * @return
	 * @throws TaleCodeException
	 */
	public static History<?> parseFile(Path path, boolean antlrConsoleReport) throws TaleCodeException {
		History<?> hist = null;
		String fileContent = "";
		try {
			fileContent = new String(Files.readAllBytes(path));
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (fileContent != null && fileContent.length() > 0 && fileContent.contains("tale")) {
			hist = TaleCodeParser.parse(fileContent, antlrConsoleReport);
		} else {
			System.err.println("File is not a tale!");
		}
		return hist;
	}

	/**
	 * Parses a string as a TaleCode string and generates a ATE history.
	 * This resets the parser after parsing.
	 * 
	 * @param code
	 *            The code to execute
	 * @param antlrConsoleReport
	 *            set to true to spit all errors that Antlr generates to console.
	 * @return
	 * @throws TaleCodeException
	 */
	public static History<?> parse(String code, boolean antlrConsoleReport) throws TaleCodeException {
		History<?> h = null;

		// Lex and syntax error reporters
		TaleCodeErrorReport lexError = TaleCodeErrorReport.LEX;
		TaleCodeErrorReport syntaxError = TaleCodeErrorReport.SYNTAX;

		// Pre parsing or first pass
		AntlrTaleCodeGrammar_v3preParser preparser = null;
		try {
			preparser = new AntlrTaleCodeGrammar_v3preParser(
					AntlrUtil.getTokenStream(code,
							AntlrTaleCodeGrammar_v3preLexer.class, lexError));
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(
					null,
					"CRITICAL ERROR!\n" + e.getMessage(),
					"FATAL",
					JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		} finally {
			resetErrors();
		}

		// Removes console error listener
		if (!antlrConsoleReport)
			preparser.getErrorListeners().clear();

		// Install the syntatic error reporter
		preparser.addErrorListener(syntaxError);

		// Call the pre parse action
		preparser.history();

		// Don't continue parsing uppon lex errors or syntatic errors
		if (TaleCodeErrorReport.hasErrors())
			throw new TaleCodeException(syntaxError.errors(), lexError.errors());

		// Final parsing
		AntlrTaleCodeGrammar_v3Parser parser = null;
		try {
			parser = new AntlrTaleCodeGrammar_v3Parser(
					AntlrUtil.getTokenStream(code,
							AntlrTaleCodeGrammar_v3Lexer.class));
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | SecurityException e) {
			throw new TaleCodeException("Interpreter Reflection exception.\n" + e.getMessage());
		} finally {
			resetErrors();
		}

		// Removes console error listener
		if (!antlrConsoleReport)
			parser.getErrorListeners().clear();

		// Second parse and generation after syntatic and lexical errors have
		// been corrected
		parser.history();

		// Build exception
		TaleCodeException parsingException = null;

		// Quit on semantic errors and report them
		if (SemanticUtil.hasErrors()) {
			HashMap<String, Integer> semanticErrors = new HashMap<>(SemanticUtil.errors());
			reset();
			parsingException = new TaleCodeException(semanticErrors);
		}

		// Quit on interpreter errors
		if (Interpreter.hasErrors()) {
			HashMap<String, Integer> interpreterErrors = new HashMap<>(Interpreter.errors());
			reset();
			if (parsingException == null)
				parsingException = new TaleCodeException(interpreterErrors);
			else
				parsingException.addErrors(interpreterErrors);
		}

		// Throw the generated exception if any
		if (parsingException != null)
			throw parsingException;

		// Return the history generated
		h = Interpreter.getHistory();

		// Reset all utils and statics
		reset();
		resetErrors();

		return h;
	}

	/**
	 * Parses a string as a TaleCode string and generates a ATE history.
	 * 
	 * @param code
	 *            The code to execute
	 * @return
	 * @throws TaleCodeException
	 */
	public static History<?> parse(String code) throws TaleCodeException {
		return parse(code, false);
	}

	public static void reset() {
		// Reset all utils and statics
		Interpreter.reset();
		SemanticUtil.reset();
	}

	public static void resetErrors() {
		TaleCodeErrorReport.reset();
	}

	public static void parseAndPlay(String code) throws TaleCodeException {
		TestUtil.play(parse(code), "CompilerPlayer");
	}
}
