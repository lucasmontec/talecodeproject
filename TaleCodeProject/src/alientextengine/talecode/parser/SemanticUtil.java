package alientextengine.talecode.parser;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.Stack;

import org.reflections.Reflections;

import alientextengine.annotation.API;

/**
 * The semantic evaluation class. This class provides a set of methods
 * and internal static data to evaluate the semantic validity of a file
 * beeing parsed.
 * 
 * @author Lucas M Carvalhaes
 *
 */
public class SemanticUtil {

	/** To register objects globaly */
	private static HashMap<String, String>	globalDefinitions;

	/** To register scenes of the tale */
	private static HashMap<String, String>	sceneDefinitions;

	/** To check validity in call number at different scopes */
	private static Stack<ArrayList<String>>	scopeCallCheck;

	/** To register only set calls */
	private static Stack<String>	scopeAPIName;

	/** To register errors during compilation */
	private static HashMap<String, Integer>			errors;

	/**
	 * To register all the valid classes of the current exposed API.
	 * Maps simple class names to class objects.
	 */
	private static final HashMap<String, Class<?>>	alienTextEngineAPI;

	/** To register the keys that can be used for tale definitions */
	private static ArrayList<String>		validTaleDefinitions;

	/** To register the starting scene node */
	private static String					startingScene;

	/** Store the scene class for checking scene body */
	private static final String sceneClass;

	/* Construction static */
	static {
		globalDefinitions = new HashMap<>();
		sceneDefinitions = new HashMap<>();
		errors = new HashMap<>();
		alienTextEngineAPI = new HashMap<>();
		validTaleDefinitions = new ArrayList<>();
		scopeCallCheck = new Stack<>();
		scopeAPIName = new Stack<>();

		// Global scope
		enterScope("global");

		//Store the scene class
		sceneClass = "alientextengine.core.graph.SceneNode";

		/* Add all valid api classes for APIID calls on ATE */
		Reflections reflections = new Reflections("alientextengine");
		Set<Class<?>> ateAPI = reflections.getTypesAnnotatedWith(API.class, true);
		ateAPI.stream().forEach(cl -> {
			alienTextEngineAPI.put(cl.getSimpleName(), cl);
		});

		/* Valid tale defs */
		validTaleDefinitions.add("tale");
		validTaleDefinitions.add("author");
		validTaleDefinitions.add("language");
	}

	public static void reset() {
		globalDefinitions.clear();
		sceneDefinitions.clear();
		errors.clear();
		scopeCallCheck.clear();
		startingScene = null;
		// Global scope
		enterScope("global");
	}

	/**
	 * @return The api id package list
	 */
	/*
	 * public static ArrayList<String> getApiIDPackagePaths(){
	 * return apiIDPackagePaths;
	 * }
	 */

	/**
	 * Returns if a call, at the current scope, was already registered.
	 * 
	 * @param call
	 *            The call key
	 * @return True if in this current scope this call was already made.
	 */
	private static boolean isCallRegistered(String call) {
		if (scopeCallCheck.isEmpty())
			return false;
		return scopeCallCheck.peek().contains(call);
	}

	/**
	 * Register (if not previously registered) a call to an assignment.
	 * This registers to the current scope.
	 * This also already checks if the call was previously registered,
	 * if so, registers an error.
	 * Only registers calls that are 'set' (POJO Set method calls)
	 * 
	 * @param call
	 *            The call key value
	 */
	public static void registerCall(String call, int line) {
		if(scopeAPIName.isEmpty()) return;
		if(!(scopeAPIName.peek().equals("global") || scopeAPIName.peek().equals("scene")))
			if(isStandartCall(scopeAPIName.peek(), call)) return;

		if (!isCallRegistered(call))
			scopeCallCheck.peek().add(call);
		else
			error("Call was already registered: " + call + ".", line);
	}

	/**
	 * Used internally to check double calls when doing assignment maps
	 * @param scopename The scoepName
	 */
	public static void enterScope(String scopename) {
		scopeAPIName.push(scopename);
		scopeCallCheck.push(new ArrayList<String>());
	}

	/**
	 * Used internally to check double calls when doing assignment maps
	 */
	public static void leaveScope() {
		scopeCallCheck.pop();
		scopeAPIName.pop();
	}

	/**
	 * Used to store all semantic errors during parsing.
	 * 
	 * @param errorText
	 *            The error description text.
	 */
	public static void error(String errorText, int line) {
		errors.put(errorText, line);
	}

	/**
	 * @return True if semantic errors exist in the last parse activity.
	 */
	public static boolean hasErrors() {
		return errors.size() > 0;
	}

	/**
	 * Checks if a key is a valid tale definition
	 * 
	 * @param key
	 *            The key to check
	 * @return True if this key is a key valid for a tale definition
	 */
	public static boolean isValidTaleDefinition(String key) {
		return validTaleDefinitions.stream().anyMatch(validKey -> {
			return validKey.equals(key);
		});
	}

	/**
	 * @return True if the starting scene was already set.
	 */
	public static boolean isStartingSceneSet() {
		return startingScene != null;
	}

	/**
	 * Used to set the starting scene for the graph model.
	 * Won't reset the scene.
	 * 
	 * @param name
	 *            The scene name
	 */
	public static void setStartingScene(String name) {
		if (!isStartingSceneSet())
			startingScene = name;
	}

	/**
	 * Checks against all valid class declarations on the valid API ID packages.
	 * 
	 * @param id
	 *            The ID (simple class name) to be checked.
	 * @return True if there is a class with this ID
	 */
	public static boolean isValidAPIID(String id) {
		return alienTextEngineAPI.containsKey(id);
	}

	/**
	 * Checks against all valid class declarations on the valid API ID packages
	 * and returns the class that is equivalent.
	 * 
	 * @param id
	 *            The ID to be checked.
	 * @return The class found with the name.
	 */
	public static Class<?> getAPIClass(String id) {
		return alienTextEngineAPI.get(id);
	}

	/**
	 * Checks if a api call (method) exists in the api id (class).
	 * Not case sensitive and will try to call ids with "set"+callid.
	 * 
	 * @param id
	 *            The api id to find the class
	 * @param callid
	 *            The method to be checked
	 * @return True if such method exists for the class.
	 */
	public static boolean isApiCallDefinedForAPIID(String id, String callid) {
		Class<?> apiclass = getAPIClass(id);
		// No class return null
		if (apiclass == null)
			return false;
		// Preprocess the method name
		String method = "set" + callid.toLowerCase();
		// Look through all methods
		for (Method m : apiclass.getMethods()) {
			if (m.getName().toLowerCase().equals(method))
				return true;
			if (m.getName().contains(method))
				return true;
		}
		// Look without set
		for (Method m : apiclass.getMethods()) {
			if (m.getName().toLowerCase().equals(callid.toLowerCase()))
				return true;
			if (m.getName().toLowerCase().contains(callid.toLowerCase()))
				return true;
		}
		return false;
	}

	/**
	 * Will return true if the method beeing called is a normal
	 * method. If the method is a setSomething method, this will return false.
	 * @param id The object to analise
	 * @param callid The method (call) name to look for
	 * @return true if this is a set method in the desired class id.
	 */
	private static boolean isStandartCall(String id, String callid) {
		Class<?> apiclass = getAPIClass(id);
		if (apiclass == null)
			return false;
		// Look without set
		for (Method m : apiclass.getMethods()) {
			if(!m.getName().contains("set")) {
				if (m.getName().toLowerCase().equals(callid.toLowerCase()))
					return true;
				if (m.getName().toLowerCase().contains(callid.toLowerCase()))
					return true;
			}
		}
		return false;
	}

	/**
	 * Checks if the call ID is defined for the scene class.
	 * @param callid The method to check in the scene class.
	 * @return True if the method exists
	 */
	public static boolean isApiCallDefinedForScene(String callid) {
		Class<?> apiclass = null;
		try {
			apiclass = Class.forName(sceneClass);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		// No class return null
		if (apiclass == null)
			return false;
		if (callid == null || callid.length() == 0)
			return false;
		// Try with set
		String method = "set" + callid.toLowerCase();
		// Look through all methods
		for (Method m : apiclass.getMethods()) {
			if (m.getName().toLowerCase().equals(method))
				return true;
			if (m.getName().contains(method))
				return true;
		}

		// Try without set
		for (Method m : apiclass.getMethods()) {
			if (m.getName().toLowerCase().equals(callid.toLowerCase()))
				return true;
		}
		return false;
	}

	/**
	 * Will return true if this scene name is avaliable
	 * 
	 * @param name
	 *            The scene name to declare.
	 * @return Already described java...
	 */
	public static boolean canDefineScene(String name) {
		return !sceneDefinitions.containsKey(name);
	}

	/**
	 * Returns true if the scene is present in the scene map and has a body of code.
	 * 
	 * @param name
	 *            The scene name
	 * @return True if the scene is present in the scene map and has a body of code
	 */
	public static boolean isSceneDefined(String name) {
		return sceneDefinitions.containsKey(name) && sceneDefinitions.get(name) != null;
	}

	/**
	 * Call this to define a scene in the scene map.
	 * 
	 * @param name
	 *            The new scene name, must be unique or the scene won't be declared
	 * @param code
	 *            The scene code
	 */
	public static void defineScene(String name, String code) {
		if (canDefineScene(name)) {
			sceneDefinitions.put(name, code);
		}
	}

	/**
	 * Simple string representation of the error collection.
	 * 
	 * @return [error, error2, error3...]
	 */
	public static String errorList() {
		return errors.toString();
	}

	/**
	 * 
	 * @return The errors list
	 */
	public static HashMap<String, Integer> errors() {
		return errors;
	}

	/**
	 * This method will tell you if a global name is avaliable to
	 * be used in a definition.
	 * 
	 * @param name
	 *            The name to check for avaliability
	 * @return True if nothing was defined with that name before
	 */
	public static boolean canDefineGlobal(String name) {
		return !isGlobalDefined(name);
	}

	/**
	 * This method is used to define (register a definition) of a global.
	 * Globals can be items, action graphs, or tale information.
	 * This method won't define a global if something is already defined to
	 * the same name (key). Use {@link #canDefineGlobal(String) canDefine} to
	 * check name avaliability.
	 * 
	 * @param name
	 *            The name to define the global
	 * @param code
	 *            The code of the entire definition
	 */
	public static void defineGlobal(String name, String code) {
		if (canDefineGlobal(name))
			globalDefinitions.put(name, code);
	}

	/**
	 * This method returns true if a global with the name parameter is defined and
	 * is not null.
	 * 
	 * @param name
	 *            The global name ID
	 * @return True if the ID is defined to something
	 */
	public static boolean isGlobalDefined(String name) {
		return globalDefinitions.containsKey(name) && globalDefinitions.get(name) != null;
	}

}
