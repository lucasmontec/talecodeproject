package alientextengine.talecode.error;

import java.util.HashMap;
import java.util.Map.Entry;

public class TaleCodeException extends Exception {
	private static final long	serialVersionUID	= -4685150945019003524L;

	private final HashMap<String, Integer>	errors				= new HashMap<>();

	public TaleCodeException(String message) {
		super(message);
	}

	@SafeVarargs
	public TaleCodeException(HashMap<String, Integer>... errorMaps) {
		super(errorsMessage(errorMaps));
		for (HashMap<String, Integer> err : errorMaps) {
			errors.putAll(err);
		}
	}

	public void addErrors(HashMap<String, Integer> errorMap) {
		errors.putAll(errorMap);
	}

	public HashMap<String, Integer> getErrors() {
		return errors;
	}

	private static String errorsMessage(HashMap<String, Integer> errorMaps[]) {
		HashMap<String, Integer> total = new HashMap<>();
		for (HashMap<String, Integer> err : errorMaps) {
			total.putAll(err);
		}
		String message = "";
		int size = total.size();
		for (Entry<String, Integer> e : total.entrySet()) {
			// If there is a valid line associated with this error, add it
			if (e.getValue() > 0)
				message += "At line " + e.getValue() + ", ";

			// Add the erro key (message)
			message += e.getKey();

			if (--size > 0) {
				message += "\n";
			}
		}
		return message;
	}
}
