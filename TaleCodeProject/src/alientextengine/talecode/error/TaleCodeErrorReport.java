package alientextengine.talecode.error;

import java.util.HashMap;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class TaleCodeErrorReport extends BaseErrorListener {

	public static TaleCodeErrorReport				SYNTAX		= new TaleCodeErrorReport(true);
	public static TaleCodeErrorReport				LEX			= new TaleCodeErrorReport(false);

	public final static HashMap<String, Integer>	errors		= new HashMap<>();

	private boolean									syntatic	= false;

	/**
	 * 
	 * @param mode
	 *            if true will report syntatic errors, if false will report lexical errors
	 */
	public TaleCodeErrorReport(boolean mode) {
		syntatic = mode;
	}

	@Override
	public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line,
			int charPositionInLine, String msg, RecognitionException e) {

		if (syntatic) {
			// Get the wrong word
			String offender = msg.substring(msg.indexOf("'") + 1).replace("\\n", "").replace("\\t", "")
					.replace("'", "").trim();
			if (msg.contains("expecting"))
				offender = offender.substring(0, offender.indexOf("expecting")).trim();

			if (errors.size() == 0)
				errors.put("Found unexpected word: " + offender + ". Possible problem: "
						+ msg, line);
		} else {
			String token = msg.substring(msg.indexOf('\'') + 1, msg.lastIndexOf('\''));
			token = token.replace("<EOF>", "EOF");

			if (!token.equals("EOF"))
				token = token.substring(0, 1);

			errors.put("Undefined symbol [ " + token + " ].", line);
		}

	}

	/**
	 * @return The errors map (error -> line).
	 */
	public HashMap<String, Integer> errors() {
		return errors;
	}

	public static boolean hasErrors() {
		return errors.size() > 0;
	}


	public static void reset() {
		errors.clear();
	}

}
