package alientextengine.talecode.talecodedoc;

import java.beans.Introspector;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.reflections.Reflections;

import alientextengine.AlienTextEngine;
import alientextengine.annotation.API;
import alientextengine.annotation.APIHidden;
import alientextengine.core.history.Scene;
import alientextengine.model.general.Actor;

public class TalecodeDoc {

	/**
	 * Simple category names for alien text engine things
	 */
	static ArrayList<String> category_types = new ArrayList<>();

	static {
		category_types.addAll(
				Arrays.asList(
						"triggers",
						"actions",
						"item",
						"streamreader",
						"retrievers",
						"designation",
						"conditions"));
	}

	/**
	 * Generates the tale code documentation using the {@link API} annotations.
	 *
	 * @return
	 */
	public static String jsonTaleCodeDoc() {
		return jsonTaleCodeDoc(true);
	}

	/**
	 * Generates the tale code documentation using the {@link API} annotations.
	 * Ritch adds the parameter/constructor parameter names after parameter types
	 * Ritch only works when ATE is compiled with the -g javac parameter. This
	 * parameter stores method parameter names after compilation. In eclipse
	 * right click the ATE project. Go to properties>java compiler and under
	 * class file generation, select: store information about method parameters.
	 *
	 * @param ritch
	 * @return
	 */
	public static String jsonTaleCodeDoc(boolean ritch) {
		String ret = "";
		Reflections reflections = new Reflections("alientextengine");
		Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(API.class, true);
		ret += "{\"AlienTextEngine\":\"" + AlienTextEngine.VERSION + "\",\n" + "\"TaleCodeDoc\":[";
		int size = annotated.size();
		for (Class<?> c : annotated) {
			ret += "\n\t" + makeJsonItem(c, ritch);
			if (--size != 0) {
				ret += ",";
			}
		}
		ret += "\n]}";
		return ret;
	}

	private static String makeJsonItem(Class<?> c, boolean ritch) {
		String ret = "";

		// Get data
		String apiid = c.getSimpleName();
		String superclass = c.getSuperclass().getSimpleName();
		String description = c.getAnnotation(API.class).description().replace("\n", "").replace("\r", "");
		String type = category_types.stream().filter(iconName -> c.getPackage().getName().contains(iconName))
				.findFirst().orElse("none");

		// Build constructor list
		String constructors = "";
		int size = c.getConstructors().length;
		for (Constructor<?> cons : c.getConstructors()) {
			String constructor = "\t\t{";

			String params;
			if (ritch) {
				params = Arrays.stream(cons.getParameters())
						.map(p -> p.getType().getSimpleName() + " " + p.getName())
						.collect(Collectors.joining(", "));
			} else {
				params = Arrays.stream(cons.getParameters()).map(p -> p.getType().getSimpleName())
						.collect(Collectors.joining(", "));
			}
			constructor += "\"parameters\":\"" + (params.length() == 0 ? "Default Constructor" : params)
					+ "\"";
			constructor += "}";
			if (--size != 0) {
				constructor += ",";
			}
			constructors += constructor + "\n";
		}

		// Build method list
		String methods = "";
		List<Method> meths = Arrays.asList(c.getDeclaredMethods()).stream()
				.filter(m -> m.toString().contains("public"))
				.filter(m -> !m.isAnnotationPresent(APIHidden.class)).collect(Collectors.toList());
		size = meths.size();
		for (Method meth : meths) {
			String params;
			if (ritch) {
				params = Arrays.stream(meth.getParameters())
						.map(p -> p.getType().getSimpleName() + " " + p.getName())
						.collect(Collectors.joining(", "));
			} else {
				params = Arrays.stream(meth.getParameters()).map(p -> p.getType().getSimpleName())
						.collect(Collectors.joining(", "));
			}
			String method = "\t\t{";
			method += "\"name\":\"" + meth.getName() + "\",";
			method += "\"parameters\":\"" + params + "\"";
			method += "}";
			if (--size != 0) {
				method += ",";
			}
			methods += method + "\n";
		}

		// Build
		ret += "{\n";
		ret += "\t\"apiid\":\"" + apiid + "\",";
		ret += "\n\t\"superclass\":\"" + superclass + "\",";
		ret += "\n\t\"icon\":\"" + type + "\",";
		ret += "\n\t\"description\":\"" + description + "\",";
		ret += "\n\t\"constructors\": [\n" + constructors + "\t],";
		ret += "\n\t\"methods\": [\n" + methods + "\t]\n";
		ret += "\t}";

		return ret;
	}

	public static String makeAceEditorModeDoc() {
		String ret = AceEditorTemplateProvider.getAceModeTemplate();

		Reflections reflections = new Reflections("alientextengine");
		String keywords = "define|end|scene|install";
		Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(API.class, true);
		String classes = "";
		String methods = "";
		int size = annotated.size();
		boolean addedMethods = false;
		for (Class<?> c : annotated) {
			classes += c.getSimpleName();

			// Build method list
			List<Method> meths = Arrays.asList(c.getDeclaredMethods()).stream()
					.filter(m -> m.toString().contains("public")).collect(Collectors.toList());
			int size2 = meths.size();
			addedMethods = size2 > 0;
			if (addedMethods) {
				for (Method meth : meths) {
					if (!methods.contains(meth.getName())) {
						methods += meth.getName();

						// if the method is a setter, talecode supports the removal of the set word and the lowercase form
						if (meth.getName().startsWith("set")
								&& Character.isUpperCase(meth.getName().replace("set", "").charAt(0))) {
							// If so, add the valid variant
							methods += "|" + Introspector.decapitalize(meth.getName().substring(3));
						}

						if (size2 - 1 != 0) {
							methods += "|";
						}
					}
					size2--;
				}
			}

			if (--size != 0) {
				classes += "|";
				if (addedMethods) {
					methods += "|";
				}
			}
		}

		// Get extra methods that need to go
		// Build method list
		Set<Class<?>> extras = new HashSet<>();
		// Other classes that need methods documented
		extras.add(Scene.class);
		extras.add(Actor.class);
		size = extras.size();
		for (Class<?> c : extras) {
			List<Method> meths = Arrays.asList(c.getDeclaredMethods()).stream()
					.filter(m -> m.toString().contains("public")).collect(Collectors.toList());
			int size2 = meths.size();
			addedMethods = size2 > 0;
			for (Method meth : meths) {
				if (!methods.contains(meth.getName())) {
					methods += meth.getName();

					// if the method is a setter, talecode supports the removal of the set word and the lowercase form
					if (meth.getName().startsWith("set")
							&& Character.isUpperCase(meth.getName().replace("set", "").charAt(0))) {
						// If so, add the valid variant
						methods += "|" + Introspector.decapitalize(meth.getName().substring(3));
					}

					if (size2 - 1 != 0) {
						methods += "|";
					}
				}
				size2--;
			}
			if (--size != 0) {
				classes += "|";
				if (addedMethods) {
					methods += "|";
				}
			}
		}

		// Look away please!
		methods = methods.replace("||", "|").replace("|||", "|").replace("||||", "|");

		ret = ret.replace("$CLASSES", classes);
		ret = ret.replace("$KEYWORDS", keywords);
		ret = ret.replace(
				"$CONSTANTS",
				"startingScene|add|tale|author|language|self|visible|true|unlisted|false|invisible");
		ret = ret.replace("$OPERATORS", "->|<->");
		ret = ret.replace("$METHODS", methods);
		return ret;
	}
}
