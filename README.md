# Tale Code #
####A Tale Language for the Alien Text Engine


 To see a preview of the Tale Code language, go to the downloads page and get the scratchpad and the CaioUrso.tale example.
 Check out [TaleDoc](http://zumbi.co.nf/TalecodeDoc/) for a automatically generated online documentation

### What is this repository for? ###

* This is a language for creating interactive histories in the Alien Text Engine.
* Version 1